// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "GAS_GideonGameMode.generated.h"

UCLASS(minimalapi)
class AGAS_GideonGameMode : public AGameModeBase
{
	GENERATED_BODY()

public:
	AGAS_GideonGameMode();
};



