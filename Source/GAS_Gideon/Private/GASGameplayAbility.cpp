// Fill out your copyright notice in the Description page of Project Settings.


#include "GASGameplayAbility.h"

FGameplayAbilityInfo UGASGameplayAbility::GetAbilityInfo()
{
	const UGameplayEffect* CooldownEffect = GetCooldownGameplayEffect();
	const UGameplayEffect* CostEffect = GetCostGameplayEffect();

	if (CooldownEffect && CostEffect)
	{
		float CooldownDuration = 0.0f;
		TArray<float> Cost;
		TArray<FString> CostName;

		Cost.Empty();
		CostName.Empty();

		CooldownEffect->DurationMagnitude.GetStaticMagnitudeIfPossible(GetAbilityLevel(), CooldownDuration);

		if (CostEffect->Modifiers.Num() > 0)
		{
			float SingleCost = 0.0f;
			for (auto EffectInfo : CostEffect->Modifiers)
			{
				EffectInfo.ModifierMagnitude.GetStaticMagnitudeIfPossible(GetAbilityLevel(), SingleCost);
				const FGameplayAttribute CostAttribute = EffectInfo.Attribute;
				FString SingleCostName = CostAttribute.AttributeName;
				Cost.Add(SingleCost);
				CostName.Add(SingleCostName);
			}
		}

		return FGameplayAbilityInfo(CooldownDuration, Cost, CostName);
	}
	else
	{
		return FGameplayAbilityInfo();
	}
}
